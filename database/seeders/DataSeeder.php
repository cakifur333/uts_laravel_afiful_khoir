<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\data;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        data::create([
            'nama'=>'Rangga',
            'job'=>'Manager'
        ]);
        data::create([
            'nama'=>'Wulan',
            'job'=>'Asisten Manager'
        ]);
        data::create([
            'nama'=>'The rock',
            'job'=>'Bodyguard'
        ]);
    }
}
