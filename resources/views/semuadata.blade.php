<table border="1" cellpadding="5" width="60%" style="margin-left:auto;margin-right:auto;">
    <tr bgcolor="#6495ED">
        <th>ID</th>
        <th>Nama</th>
        <th>Job</th>
        <th>Opsi</th>
    </tr>
    @foreach ($alldata as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td><a href="{{ url('tampil', $item->id) }}">{{ $item->nama }}</a></td>
            <td>{{ $item->job }}</td>
            <td>
                <a href="{{ url('editdata', $item->id) }}">Edit</a>
                <a href="{{url('delete', $item->id)}}">Hapus</a>
            </td>
        </tr>
    @endforeach
</table>
<button style="margin-left: 20%"><a href="buatdata">Tambah</a></button>