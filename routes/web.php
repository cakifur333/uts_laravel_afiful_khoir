<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;


Route::get('/', function () {
    return view('konten');
});

Route::get('tampil',[DataController::class, 'index']);

Route::get('tampil/{id}',[DataController::class, 'show']);

Route::get('buatdata',[DataController::class, 'create']);
Route::post('simpandata',[DataController::class, 'store']);

Route::get('editdata/{id}',[DataController::class, 'edit']);
Route::post('update/{id}',[DataController::class, 'update']);

Route::get('delete/{id}',[DataController::class, 'destroy']);